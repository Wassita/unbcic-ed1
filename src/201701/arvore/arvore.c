#include <stdlib.h>
#include <stdio.h>

typedef struct no{
    int dado;

    struct no* esq;
    struct no* dir;
} t_no;


//Assinaturas de Funções
t_no* cria_no(int dado);

t_no* inserir(t_no* raiz, int dado);

void  pre_ordem(t_no* raiz);
void  pos_ordem(t_no* raiz);
void  em_ordem(t_no* raiz);






//Implementações das assinaturas
t_no* cria_no(int dado){
    t_no* resultado = (t_no *) malloc(sizeof(t_no));

    resultado->dado = dado;
    resultado->esq  = NULL;
    resultado->dir  = NULL;

    return resultado;

}//end cria_no


t_no* inserir(t_no* raiz, int dado){
    
    if(raiz == NULL){
        return cria_no(dado);
    }else{
        
        if(raiz->dado > dado)
            raiz->esq = inserir(raiz->esq, dado);
        else if(raiz->dado < dado)
            raiz->dir = inserir(raiz->dir, dado);
    }

    return raiz;

}//end inserir


void pre_ordem(t_no* raiz){
    if( raiz != NULL){
        printf("%d\n", raiz->dado);

        pre_ordem(raiz->esq);
        pre_ordem(raiz->dir);
    }
}

void pos_ordem(t_no* raiz){
    if( raiz != NULL){
        pos_ordem(raiz->esq);
        pos_ordem(raiz->dir);

        printf("%d\n", raiz->dado);
    }
}

void  em_ordem(t_no* raiz){
    if( raiz != NULL){
        em_ordem(raiz->esq);
        printf("%d\n", raiz->dado);
        em_ordem(raiz->dir);
    }
}


int main(){

    t_no* raiz = NULL;

    raiz = inserir(raiz, 50);
    raiz = inserir(raiz, 17);
    raiz = inserir(raiz, 72);
    raiz = inserir(raiz, 12);
    raiz = inserir(raiz, 23);
    raiz = inserir(raiz, 54);
    raiz = inserir(raiz, 76);
    raiz = inserir(raiz, 9);
    raiz = inserir(raiz, 14);
    raiz = inserir(raiz, 19);
    raiz = inserir(raiz, 67);

    printf("\nPre-ordem\n");
    pre_ordem(raiz);

    printf("\nPos-ordem\n");
    pos_ordem(raiz);

    printf("\nEm-ordem\n");
    em_ordem(raiz);

}













