#include <stdlib.h>
#include <stdio.h>

#include "arvore.h"





//Implementações das assinaturas
t_no* cria_no(int dado){
    t_no* resultado = (t_no *) malloc(sizeof(t_no));

    resultado->dado = dado;
    resultado->esq  = NULL;
    resultado->dir  = NULL;

    return resultado;

}//end cria_no


t_no* inserir(t_no* raiz, int dado){
    
    if(raiz == NULL){
        return cria_no(dado);
    }else{
        
        if(raiz->dado > dado)
            raiz->esq = inserir(raiz->esq, dado);
        else if(raiz->dado < dado)
            raiz->dir = inserir(raiz->dir, dado);
    }

    return raiz;

}//end inserir


void pre_ordem(t_no* raiz){
    if( raiz != NULL){
        printf("%d\n", raiz->dado);

        pre_ordem(raiz->esq);
        pre_ordem(raiz->dir);
    }
}

void pos_ordem(t_no* raiz){
    if( raiz != NULL){
        pos_ordem(raiz->esq);
        pos_ordem(raiz->dir);

        printf("%d\n", raiz->dado);
    }
}

void  em_ordem(t_no* raiz){
    if( raiz != NULL){
        em_ordem(raiz->esq);
        printf("%d\n", raiz->dado);
        em_ordem(raiz->dir);
    }
}
















