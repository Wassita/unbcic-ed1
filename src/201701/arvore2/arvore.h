
#ifndef __ARVORE_H__
#define __ARVORE_H__

typedef struct no{
    int dado;

    struct no* esq;
    struct no* dir;
} t_no;


//Assinaturas de Funções
t_no* cria_no(int dado);

t_no* inserir(t_no* raiz, int dado);

void  pre_ordem(t_no* raiz);
void  pos_ordem(t_no* raiz);
void  em_ordem(t_no* raiz);
void  em_largura(t_no* raiz);

#endif
