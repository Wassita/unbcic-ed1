
#ifndef __FILA_H__
#define __FILA_H__

#include "arvore.h"

typedef struct fila{
    t_no **item;
    int tamanho;
    int inicio, fim;
} t_fila;


//Assinaturas de Funções
t_fila* getFila(int tamanho);
void    liberaFila(t_fila* fila);
void    inserir(t_fila* fila, t_no* valor);
t_no*   remover(t_fila* fila);

#endif
