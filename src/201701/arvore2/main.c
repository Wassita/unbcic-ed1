#include <stdio.h> 
#include "arvore.h"
#include "fila.h"


int main(){

    t_no* raiz = NULL;

    raiz = inserir(raiz, 50);
    raiz = inserir(raiz, 17);
    raiz = inserir(raiz, 72);
    raiz = inserir(raiz, 12);
    raiz = inserir(raiz, 23);
    raiz = inserir(raiz, 54);
    raiz = inserir(raiz, 76);
    raiz = inserir(raiz, 9);
    raiz = inserir(raiz, 14);
    raiz = inserir(raiz, 19);
    raiz = inserir(raiz, 67);

    printf("\nPre-ordem\n");
    pre_ordem(raiz);

    printf("\nPos-ordem\n");
    pos_ordem(raiz);

    printf("\nEm-ordem\n");
    em_ordem(raiz);

    printf("\nEm-Largura\n");
    em_largura(raiz);
    

}





